using UnityEngine;

public class ButtonsMenu : MonoBehaviour
{
    public int CurrentTimeFlow { get; private set; } = 1;
    public bool TimeGrowing { get; private set; } = true;

    public void ReverseTimeFlow() =>
        TimeGrowing = !TimeGrowing;

    public void NormalTime() =>
        CurrentTimeFlow = 1;

    public void FiftyTimesFaster() =>
        CurrentTimeFlow = 50;

    public void HundredFiftyTimesFaster() =>
        CurrentTimeFlow = 150;

    public void FiveHundredsTimesFaster() =>
        CurrentTimeFlow = 500;
}
