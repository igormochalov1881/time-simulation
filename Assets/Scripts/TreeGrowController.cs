using UnityEngine;

public class TreeGrowController : MonoBehaviour
{
    [SerializeField] private Timer _timer;
    [SerializeField] private Animator _treeAnimator;
    [Range(10, 60)]
    [SerializeField] private int _minutesPerStage;

    private readonly string animationStage = "Stage";

    private int stage;

    private void Update()
    {
        int stageScore = (_timer.Minutes / _minutesPerStage) % 6;

        stage = stageScore > 0 && stageScore < 6 ? stageScore : 0;

        SetTreeVisibility();
        SetAnimationStage();
    }

    private void SetTreeVisibility()
    {
        if (stage != 0)
            _treeAnimator.gameObject.SetActive(true);
        else
            _treeAnimator.gameObject.SetActive(false);
    }

    private void SetAnimationStage()
    {
        switch (stage)
        {
            case 1:
                _treeAnimator.SetFloat(animationStage, 1);
                break;
            case 2:
                _treeAnimator.SetFloat(animationStage, 2); 
                break;
            case 3:
                _treeAnimator.SetFloat(animationStage, 3);
                break;
            case 4:
                _treeAnimator.SetFloat(animationStage, 4);
                break;
            case 5:
                _treeAnimator.SetFloat(animationStage, 5);
                break;
        }
    }
}
