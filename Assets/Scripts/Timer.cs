using UnityEngine;
using UnityEngine.UI;

public class Timer : MonoBehaviour
{
    [SerializeField] private ButtonsMenu _menu;
    [SerializeField] private Text _timerText;

    public int Seconds { get; private set; }
    public int Minutes { get; private set; }
    public int Hours { get; private set; }
    public int Days { get; private set; }
    public float CurrentTimer { get; private set; }
    public int Multiplier { get; private set; }

    private int visibleSeconds;
    private int visibleMinutes;
    private int visibleHours;
    private int visibleDays;

    private void Update()
    {
        Multiplier = _menu.CurrentTimeFlow;

        CountTime();
        UpdateTimerText();
    }

    private void CountTime()
    {
        CurrentTimer = _menu.TimeGrowing ? 
            CurrentTimer + (Time.deltaTime * Multiplier) :
            CurrentTimer - (Time.deltaTime * Multiplier);

        if (CurrentTimer < 0) CurrentTimer = 0;

        UpdateSeconds();
        UpdateMinutes();
        UpdateHours();
        UpdateDays();
    }

    private void UpdateSeconds()
    {
        int maximumVisibleSeconds = 60;

        Seconds = (int)CurrentTimer;
        visibleSeconds = Seconds % maximumVisibleSeconds;
    }

    private void UpdateMinutes()
    {
        int secondsInMinute = 60, maximumVisibleMinutes = 60;

        Minutes = Seconds / secondsInMinute;
        visibleMinutes = Minutes % maximumVisibleMinutes;
    }

    private void UpdateHours()
    {
        int secondsInHour = 3600, maximumVisibleHours = 24;

        Hours = Seconds / secondsInHour;
        visibleHours = Hours % maximumVisibleHours;
    }

    private void UpdateDays()
    {
        var secondsInDay = 86400;

        Days = Seconds / secondsInDay;
        visibleDays = Days;
    }

    private void UpdateTimerText() => 
        _timerText.text = $"{visibleDays} : {visibleHours} : {visibleMinutes} : {visibleSeconds}";
}